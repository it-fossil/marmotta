FROM stackbrew/ubuntu:14.04
MAINTAINER "TAYAA Med Amine" <http://github.com/shivha>

ENV TOMCAT_VERSION 7.0.63
ENV MARMOTTA_VERSION 3.3.0

# Update and upgrade packages.
RUN apt-get update && \
    apt-get upgrade -y -o DPkg::Options::=--force-confold

# Install build dependencies.
RUN apt-get install -y --force-yes curl wget openjdk-7-jre-headless

# Get tomcat.
RUN wget http://apache.mirrors.lucidnetworks.net/tomcat/tomcat-7/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
    tar xvzf apache-tomcat-${TOMCAT_VERSION}.tar.gz && \
    mv apache-tomcat-${TOMCAT_VERSION} /usr/local/tomcat && \
    rm -rf apache-tomcat-${TOMCAT_VERSION}.tar.gz /usr/local/tomcat/webapps/*

# Get marmotta.
RUN wget http://apache.osuosl.org/marmotta/${MARMOTTA_VERSION}/apache-marmotta-${MARMOTTA_VERSION}-webapp.tar.gz && \
    tar xvzf apache-marmotta-${MARMOTTA_VERSION}-webapp.tar.gz && \
    cp /apache-marmotta-${MARMOTTA_VERSION}/marmotta.war /usr/local/tomcat/webapps && \
    rm -rf apache-marmotta-${MARMOTTA_VERSION}-webapp.tar.gz /apache-marmotta-${MARMOTTA_VERSION}

# Configure marmotta access
RUN /usr/local/tomcat/bin/startup.sh && sleep 10 && \
    curl -v -X POST -H "Content-Type: application/json" -d '[":plain::pass123"]' http://localhost:8080/marmotta/config/data/user.admin.pwhash && sleep 10 && \
    /usr/local/tomcat/bin/shutdown.sh && sleep 10
RUN /usr/local/tomcat/bin/startup.sh && sleep 10 && \
    curl -v -X POST -H "Content-Type: application/json" -d '["standard"]' http://localhost:8080/marmotta/config/data/security.profile -u admin:pass123 && sleep 10 && \
    /usr/local/tomcat/bin/shutdown.sh && sleep 10

EXPOSE 8080

CMD /usr/local/tomcat/bin/startup.sh && tail -f /usr/local/tomcat/logs/catalina.out
